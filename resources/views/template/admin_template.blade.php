<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Desafio Tributei</title>
	
	<!-- Biblioteca React (teste) -->
			<!-- <script src="https://unpkg.com/react@17/umd/react.development.js" crossorigin></script> 
		 <script src="https://unpkg.com/react-dom@17/umd/react-dom.development.js" crossorigin></script> -->

	<!-- Jquery e Plugins js-->
  <link href="{{asset('assets/css/main.css')}}" rel="stylesheet" />

	<!-- <link href="{{asset('assets/js/select2-develop/dist/css/select2.min.css')}}" rel="stylesheet" /> -->
	<!-- <script src="{{asset('assets/dropzone-5.7.0/dist/dropzone.js')}}"></script> -->
	<!-- <script type="text/javascript" src = "{{asset('assets/js/jQuery-Mask-Plugin/src/jquery.mask.js')}}"></script>
	  <script src="{{asset('assets/js/jquery-maskmoney/src/jquery.maskMoney.js')}}" type="text/javascript"></script> -->

	  <!-- Bootstrap -->
	<link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
		<!-- <script type="text/javascript"  src="{{asset('assets/js/popper.min.js')}}">	</script> -->
    <script type="text/javascript" src="{{asset('assets/js/jquery-3.6.0.min.js')}}"></script>

	<script type="text/javascript" src="{{asset('assets/js/bootstrap.min.js')}}"></script>

	<!-- Css Pessoal e Icones -->

	<!-- <link rel="stylesheet" href="{{asset('assets/css/all.css')}}"> -->
	<link rel="stylesheet" href="{{asset('assets/css/simple_sidebar.css')}}"></link>

	<!-- <script type="text/javascript"  src="{{asset('assets/js/main.js')}}">	</script> -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<style>

</style>
</head>

<body>
	<main >
<div class="d-flex" id="wrapper">


		<div class="d-flex flex-column p-3 text-white bg-dark" style="width: 280px;" id="sidebar-wrapper">
  <a href="#" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-white text-decoration-none
   ">
    <svg class="bi me-2" width="40" height="32"><use xlink:href="#bootstrap"></use></svg>
    <!-- <img src="{{asset('img/tech_contru/logo.png')}}" class = 'w-100'> -->
  </a>
  <hr>
  <ul class="nav nav-pills flex-column mb-auto">
    <li class="nav-item">
      <a href="{{asset('/home')}}" class="nav-link text-white {{ Request::segment(1) == 'home' ? 'active' : '' }}">
        <i class="fas fa-home"></i> &nbsp;&nbsp;&nbsp;Upload Arquivo
      </a>
    </li>
    <li>

      <a href="{{asset('notas')}}" class="nav-link text-white {{ Request::segment(1) == 'notas' ? 'active' : '' }}">
        <i class="fas fa-users"></i> &nbsp;&nbsp;&nbsp;NFes
      </a>
    </li>

    <li>

<a href="{{asset('relatorios')}}" class="nav-link text-white {{ Request::segment(1) == 'relatorios' ? 'active' : '' }}">
  &nbsp;&nbsp;&nbsp;Relátorio
</a>
</li>

<li>

<a href="{{asset('logout')}}" class="nav-link text-white">
  &nbsp;&nbsp;&nbsp;Sair
</a>
</li>
</div>
      <!--barra de navegação-->
      
	<div id="page-content-wrapper" style="overflow:	hidden;">  
<div class="container-fluid " >

<div class="d-flex align-items-center justify-content-between p-3 mb-4 my-3  bg-dark rounded shadow-sm text-white">
    @yield('title')
<!--       <small>Since 2011</small>
 -->    
 		

				  <button class="btn  dropdown-toggle" type="button" disabled>
				    <i class="fas fa-cog"></i> &nbsp;&nbsp;&nbsp;
				  </button>
				  
				  </div>
  </div>

@if(session()->has('status'))

     <div class="alert alert-success alert-dismissible fade show" role="alert">
  {{session('status')}}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif
@if (\Session::has('errors'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
  {{session('errors')}}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
    @endif
@yield('content')

			



			</div>
		</div>
	</div>
	</main>
</body>