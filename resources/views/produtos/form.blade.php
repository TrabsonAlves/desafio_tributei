@extends('template.admin_template')
@section('title')
<div>
<h3>Produtos</h3>

<small>Show</small>
</div>

@endsection
@section('content')
<div class="container">
<form class="form-group" method='POST' action='/notas/produtos/{{$produto->id}}'>
@csrf
{{ method_field('PUT') }}

  <div class="row">
    <div class="col-md-6 mb-3">
      <label for="codigo_produto">Código Produto</label>
      <input type="text" class="form-control" id="codigo_produto" name="codigo_produto"  value="{{$produto->codigo_produto}}" >
      
    </div>
    <div class="col-md-6 mb-3">
      <label for="nome_produto">Nome Produto</label>
      <input type="text" class="form-control" id="nome_produto" name="nome_produto"  value="{{$produto->nome_produto}}" >
      
    </div>
    
  </div>
  <div class="row">
    <div class="col-md-4 mb-3">
      <label for="valor_unitario">Valor Unitário</label>
      <div class="input-group">
      <div class="input-group-prepend">
          <span class="input-group-text" id="validationTooltipUsernamePrepend">R$</span>
        </div>
      <input type="number" class="form-control" id="valor_unitario" name="valor_unitario"  value="{{$produto->valor_unitario}}" >
</div>
    </div>
    <div class="col-md-4 mb-3">
      <label for="unidade_produto">Unidade Produto </label>
      <div class="input-group">
     
      <input type="text" class="form-control" id="unidade_produto" name="unidade_produto" placeholder="Ex.: un, pack" value="{{$produto->unidade_produto}}" >
      </div>

    </div>
    <div class="col-md-4 mb-3">
      <label for="valor_produto">Valor Total</label>
      <div class="input-group">
      <div class="input-group-prepend">
          <span class="input-group-text" id="validationTooltipUsernamePrepend">R$</span>
        </div>

        <input type="number" class="form-control" id="valor_produto" name="valor_produto"  value="{{$produto->valor_produto}}" >
        
    </div>
  </div>
</div>
<hr>
<div class="row">
    <div class="col-md-4 mb-3">
      <label for="base_icms">Base Calc. ICMS</label>
      <div class="input-group">
      <div class="input-group-prepend">
          <span class="input-group-text" id="validationTooltipUsernamePrepend">R$</span>
        </div>
      <input type="number" class="form-control" id="base_icms" name="base_icms"  value="{{$produto->base_icms}}" >
</div>
    </div>
    <div class="col-md-4 mb-3">
      <label for="aliq_icms">Aliq. ICMS</label>
      <div class="input-group">
      <div class="input-group-prepend">
          <span class="input-group-text" id="validationTooltipUsernamePrepend">R$</span>
        </div>
      <input type="number" class="form-control" id="aliq_icms" name="aliq_icms"  value="{{$produto->aliq_icms}}" >
      </div>

    </div>
    <div class="col-md-4 mb-3">
      <label for="valor_icms">Valor ICMS</label>
      <div class="input-group">
      <div class="input-group-prepend">
          <span class="input-group-text" id="validationTooltipUsernamePrepend">R$</span>
        </div>

        <input type="number" class="form-control" id="valor_icms" name="valor_icms"  value="{{$produto->valor_icms}}" >
        
    </div>
  </div>
</div>


  <hr>
         <p>
         <button class="btn btn-outline-primary" type="submit">Atualizar</button>
         </p>

         
</form>
</div>
</div>
@endsection