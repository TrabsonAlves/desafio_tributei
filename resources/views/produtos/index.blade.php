@extends('template.admin_template')
@section('title')
<h2>Produtos</h2>
@endsection

@section('content')
    <table class='table'>
        <thead>
        <tr>
            <th>Código</th>
            <th>Nome Produto</th>
            <th>Quantidade Comprada</th>
            <th>Valor Total</th>
            <th>Ações</th>
            </tr>
        </thead>
    <tbody>
    @foreach($produtos as $produto)
    <tr>
    <td>{{$produto->codigo_produto}}</td>
    <td>{{$produto->nome_produto}}</td>
    <td>{{$produto->quantidade_comprada}}</td>
    <td>{{$produto->valor_produto}}</td>
    <td>
    <a href="/notas/produtos/{{$produto->id}}/edit"><button class='btn btn-info'>Detalhes</button></a>
        <form method='POST' action="/notas/produtos/{{$produto->id}}" style="display: inline">
	                        {{ method_field('DELETE') }}
	                        {{ csrf_field() }}
                            <button class='btn btn-danger'>Deletar</button></a>  
</form>
    </td>
    </tr>
    @endforeach
    </tbody>
    </table>
    @endsection