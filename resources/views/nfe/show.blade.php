@extends('template.admin_template')
@section('title')
<div>
<h3>NFes</h3>

<small>Show</small>
</div>

@endsection
@section('content')
<div class="container">
<form class="form-group" method='POST' action='/notas/{{$nfe->id}}'>
@csrf
{{ method_field('PUT') }}

  <div class="row">
    <div class="col-md-6 mb-3">
      <label for="descricao">Descrição</label>
      <input type="text" class="form-control" id="descricao" name="descricao"  value="{{$nfe->descricao}}" required>
      
    </div>
    <div class="col-md-6 mb-3">
      <label for="data_emissao">Data de Emissão</label>
      <input type="date" class="form-control" id="data_emissao" name="data_emissao"  value="{{$nfe->data_emissao}}" required>
      
    </div>
    
  </div>
  <div class="row">
    <div class="col-md-4 mb-3">
      <label for="valor_produtos">Valor Produtos</label>
      <div class="input-group">
      <div class="input-group-prepend">
          <span class="input-group-text" id="validationTooltipUsernamePrepend">R$</span>
        </div>
      <input type="number" class="form-control" id="valor_produtos" name="valor_produtos"  value="{{$nfe->valor_produtos}}" required>
</div>
    </div>
    <div class="col-md-4 mb-3">
      <label for="valor_ICMS">Valor ICMS</label>
      <div class="input-group">
      <div class="input-group-prepend">
          <span class="input-group-text" id="validationTooltipUsernamePrepend">R$</span>
        </div>
      <input type="number" class="form-control" id="valor_ICMS" name="valor_ICMS"  value="{{$nfe->valor_ICMS}}" required>
      </div>

    </div>
    <div class="col-md-4 mb-3">
      <label for="valor_nf">Valor da Nota</label>
      <div class="input-group">
      <div class="input-group-prepend">
          <span class="input-group-text" id="validationTooltipUsernamePrepend">R$</span>
        </div>

        <input type="number" class="form-control" id="valor_nf" name="valor_nf"  value="{{$nfe->valor_nf}}" required>
        
    </div>
  </div>
</div>

<div class="row">
    <div class="col-md-4 mb-3">
      <label for="valor_IPI">Valor IPI</label>
      <div class="input-group">
      <div class="input-group-prepend">
          <span class="input-group-text" id="validationTooltipUsernamePrepend">R$</span>
        </div>
      <input type="number" class="form-control" id="valor_IPI" name="valor_IPI"  value="{{$nfe->valor_IPI}}" required>
</div>
    </div>
    <div class="col-md-4 mb-3">
      <label for="valor_PIS">Valor PIS</label>
      <div class="input-group">
      <div class="input-group-prepend">
          <span class="input-group-text" id="validationTooltipUsernamePrepend">R$</span>
        </div>
      <input type="number" class="form-control" id="valor_PIS" name="valor_PIS"  value="{{$nfe->valor_PIS}}" required>
      </div>

    </div>
    <div class="col-md-4 mb-3">
      <label for="valor_COFINS">Valor COFINS</label>
      <div class="input-group">
      <div class="input-group-prepend">
          <span class="input-group-text" id="validationTooltipUsernamePrepend">R$</span>
        </div>

        <input type="number" class="form-control" id="valor_COFINS" name="valor_COFINS"  value="{{$nfe->valor_COFINS}}" required>
        
    </div>
  </div>
</div>

<hr>
 
  <div class="row">
      <div class="col-md-5">
          <h5>Emissor</h5>
          <div><label for="nome_emit">Nome</label><input type="text" id="nome_emit" name="nome_emit" value="{{$nfe->emissor->nome}}"class="form-control"></div>
          <div><label for="nome_fantasia_emit">Nome Fantasia</label><input type="text" id="nome_fantasia_emit" name="nome_fantasia_emit" value="{{$nfe->emissor->nome_fantasia}}"class="form-control"></div>
          <div><label for="IE_emit">Inscrição Estadual</label><input type="text" id="IE_emit" name="IE_emit" value="{{$nfe->emissor->IE}}"class="form-control"></div>
          <div><label for="cnpj_emit">CNPJ</label><input type="text" id="cnpj_emit" name="cnpj_emit" value="{{$nfe->emissor->CNPJ}}"class="form-control"></div>
      </div>
      <div class="col-md-2"></div>
      <div class="col-md-5">
          <h5>Destinatário</h5>
          <div><label for="nome_dest">Nome</label><input type="text" id="nome_dest" name="nome_dest" value="{{$nfe->dest->nome}}"class="form-control"></div>
          <div><label for="nome_fantasia_dest">Nome Fantasia</label><input type="text" id="nome_fantasia_dest" name="nome_fantasia_dest" value="{{$nfe->dest->nome_fantasia}}"class="form-control"></div>
          <div><label for="IE_dest">Inscrição Estadual</label><input type="text" id="IE_dest" name="IE_dest" value="{{$nfe->dest->IE}}"class="form-control"></div>
          <div><label for="cnpj_dest">CNPJ</label><input type="text" id="cnpj_dest" name="cnpj_dest" value="{{$nfe->dest->CNPJ}}"class="form-control"></div>
      </div>
  </div>
  <hr>
         <p>
         <button class="btn btn-outline-primary" type="submit">Atualizar</button>
         <a href="/notas/produtos/{{$nfe->id}}"><button class="btn btn-outline-info" type="button">Produtos</button></a>
         </p>

         
</form>
</div>
</div>
@endsection