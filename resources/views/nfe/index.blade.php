@extends('template.admin_template')

@section('title')
<h2>NFes</h2>
@endsection

@section('content')
    <table class='table text-center'>
        <thead>
        <tr>
            <th>Descrição</th>
            <th>Itens em Nota</th>
            <th>Valor Total</th>
            <th>Emissor</th>
            <th>Destinatario</th>
            <th>Data Emissão</th>
            <th>Ações</th>
            </tr>
        </thead>
    <tbody>
    @foreach($nfes as $nfe)
    <tr>
    <td>{{$nfe->descricao}}</td>
    <td>{{$nfe->produtos->count()}}</td>
    <td>{{$nfe->valor_nf}}</td>
    <td>{{$nfe->emissor->nome_fantasia}}</td>
    <td>{{$nfe->dest->nome}}</td>
    <td>{{$nfe->data_emissao}}</td>
    <td>
        <a href="/notas/{{$nfe->id}}"><button class='btn btn-outline-info'>Detalhes</button></a>
        <a href="/notas/produtos/{{$nfe->id}}"><button class='btn btn-outline-primary'>Produtos</button></a>

        <form method='POST' action="/notas/{{$nfe->id}}" style="display: inline">
	                        {{ method_field('DELETE') }}
	                        {{ csrf_field() }}
                            <button class='btn btn-outline-danger'>Deletar</button>
                            </form>    
    </td>
    </tr>
    @endforeach
    </tbody>
    </table>
@endsection