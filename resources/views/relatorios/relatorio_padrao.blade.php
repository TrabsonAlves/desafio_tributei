<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Desafio Tributei</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<!-- Biblioteca React (teste) -->
			<!-- <script src="https://unpkg.com/react@17/umd/react.development.js" crossorigin></script> 
		 <script src="https://unpkg.com/react-dom@17/umd/react-dom.development.js" crossorigin></script> -->

	<!-- Jquery e Plugins js-->
	<script src="{{asset('assets/js/jquery-3.5.0.min.js')}}"></script>
	<!-- <link href="{{asset('assets/js/select2-develop/dist/css/select2.min.css')}}" rel="stylesheet" />
	<script src="{{asset('assets/js/select2-develop/dist/js/select2.min.js')}}"></script>
	<script type="text/javascript" src = "{{asset('assets/js/jQuery-Mask-Plugin/src/jquery.mask.js')}}"></script>
	  <script src="{{asset('assets/js/jquery-maskmoney/src/jquery.maskMoney.js')}}" type="text/javascript"></script> -->

	  <!-- Bootstrap -->
	<link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
		<!-- <script type="text/javascript"  src="{{asset('assets/js/popper.min.js')}}">	</script> -->

	<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>

	<!-- Css Pessoal e Icones -->

    <!-- <link rel="stylesheet" href="{{asset('assets/css/all.css')}}"> -->
    <link href="{{asset('assets/css/main.css')}}" rel="stylesheet" />

	<!-- <link rel="stylesheet" href="{{asset('assets/css/simple_sidebar.css')}}"></link> -->

	<!-- <script type="text/javascript"  src="{{asset('assets/js/main.js')}}">	</script> -->
	<style>
.page-break{
       position:relative;
       margin: 50% 0 ;
}
</style>
</head>

<body>
@foreach($model as $nfe)
<div class="container-rel" style=''>
                                        

<div class="row my-3" >

        
<div class=" d-flex flex-wrap justify-content-around text-center " >

     
      <div class="flex-fill w-25 border rounded {{(Arr::exists($vars,'natOP')?'':'d-none')}}" id="natOP">
       <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      NATUREZA DA OPERAÇÃO</strong>
             <p class=" " style='font-size: 15px'>{{$nfe->descricao}}</p>
      </div>        
      
</div>
   
       <div class="flex-fill w-25 border rounded {{(Arr::exists($vars,'pr_aut')?'':'d-none')}}" id="pr_aut">
        <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      PROTOCOLO DE AUTORIZAÇÃO DE USO</strong>
             <p class=" " style='font-size: 15px'></p>
      </div>        
       
</div>
   
       <div class="flex-fill w-25 border rounded {{(Arr::exists($vars,'ie_emit')?'':'d-none')}}" id="ie_emit">
        <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      INSCRIÇÃO ESTADUAL</strong>
             <p class=" " style='font-size: 15px'>{{$nfe->emissor->Ie}}</p>
      </div>        
       
</div>
   
       <div class="flex-fill w-25 border rounded {{(Arr::exists($vars,'ie_sub_emit')?'':'d-none')}}" id="ie_sub_emit">
        <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      INSCRIÇÃO ESTADUAL DO SUBST. TRIBUT</strong>
             <p class=" " style='font-size: 15px'></p>
      </div>        
       
</div>
<div class="flex-fill w-25 border rounded {{(Arr::exists($vars,'cnpj_emit')?'':'d-none')}}" id="cnpj_emit">
        <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      CNPJ</strong>
             <p class=" " style='font-size: 15px'>{{$nfe->emissor->CNPJ}}</p>
      </div>        
       
</div>
    </div>

</div>




<div class="row my-3" >

<strong> Área do Destinatário / Remetente</strong>
        
<div class=" d-flex flex-wrap justify-content-around text-center " >

     
      <div class=" cardflex-fill w-25 border rounded {{(Arr::exists($vars,'nome_dest')?'':'d-none')}}" id="nome_dest">
      <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      NOME / RAZÃO SOCIAL</strong>
             <p class=" " style='font-size: 15px'>{{$nfe->dest->nome}}</p>
      </div>       
</div>
   
       <div class="flex-fill w-25 border rounded {{(Arr::exists($vars,'cnpj_dest')?'':'d-none')}}" id="cnpj_dest">
              
       <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      CNPJ / CPF</strong>
             <p class=" " style='font-size: 15px'>{{$nfe->dest->CNPJ}}</p>
      </div> 
      
      
</div>
   
       <div class="flex-fill w-25 border rounded {{(Arr::exists($vars,'end_dest')?'':'d-none')}}" id="end_dest">
       <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      ENDEREÇO</strong>
             <p class=" " style='font-size: 15px'>{{$nfe->dest->endereco->endereco_full}}</p>
      </div> 
      
</div>
   
       <div class="flex-fill w-25 border rounded {{(Arr::exists($vars,'bairro_dest')?'':'d-none')}}" id="bairro_dest">
       <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      BAIRRO / DISTRITO</strong>
             <p class=" " style='font-size: 15px'>{{$nfe->dest->endereco->bairro}}</p>
      </div>        
       
</div>
   
       <div class="flex-fill w-25 border rounded {{(Arr::exists($vars,'cep_dest')?'':'d-none')}}" id="cep_dest">
       <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      CEP</strong>
             <p class=" " style='font-size: 15px'>{{$nfe->dest->endereco->cep}}</p>
      </div>        
       
</div>
   
       <div class="flex-fill w-25 border rounded {{(Arr::exists($vars,'saida_dest')?'':'d-none')}}" id="saida_dest">
       <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      DATA DA SAÍDA</strong>
             <p class=" " style='font-size: 15px'></p>
      </div>        
       
</div>
   
       <div class="flex-fill w-25 border rounded {{(Arr::exists($vars,'mun_dest')?'':'d-none')}}" id="mun_dest">
       <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      MUNICÍPIO</strong>
             <p class=" " style='font-size: 15px'>{{$nfe->dest->endereco->municipio}}</p>
      </div>        
       
</div>
   
       <div class="flex-fill w-25 border rounded {{(Arr::exists($vars,'uf_dest')?'':'d-none')}}" id="uf_dest">
              <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      UF</strong>
             <p class=" " style='font-size: 15px'>{{$nfe->dest->endereco->Uf}}</p>
      </div> 
</div>
   
       <div class="flex-fill w-25 border rounded {{(Arr::exists($vars,'fone_dest')?'':'d-none')}}" id="fone_dest">
              <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      FONE / FAX</strong>
             <p class=" " style='font-size: 15px'>{{$nfe->dest->endereco->fone}}</p>
      </div> 
</div>
   
       <div class="flex-fill w-25 border rounded {{(Arr::exists($vars,'ie_dest')?'':'d-none')}}" id="ie_dest">
              <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      INSCRIÇÃO ESTADUAL</strong>
             <p class=" " style='font-size: 15px'>{{$nfe->dest->IE}}</p>
      </div> 
</div>
   
       <div class="flex-fill w-25 border rounded {{(Arr::exists($vars,'hora_dest')?'':'d-none')}}" id="hora_dest">
              <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      HORA DA SAÍDA</strong>
             <p class=" " style='font-size: 15px'></p>
      </div> 
</div>
    </div>

</div>
   
<div class="row my-3" >
    <strong>CÁLCULO DO IMPOSTO</strong>
<div class=" d-flex justify-content-around text-center " >
    <div class="flex-fill border rounded {{(Arr::exists($vars,'bc_icms')?'':'d-none')}}" id="bc_icms">
    <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      BASE DE CÁLCULO DO ICMS</strong>
             <p class=" " style='font-size: 15px'></p>
      </div>        
    </div>
    <div class="flex-fill border rounded {{(Arr::exists($vars,'v_icms')?'':'d-none')}}"  id="v_icms">
    <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      VALOR DO ICMS</strong>
             <p class=" " style='font-size: 15px'>{{$nfe->valor_ICMS}}</p>
      </div>        
    </div>
    <div class="flex-fill border rounded {{(Arr::exists($vars,'bc_icms_st')?'':'d-none')}}" id="bc_icms_st">
    <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      BASE DE CÁLCULO DO ICMS S.T.</strong>
             <p class=" " style='font-size: 15px'></p>
      </div>        
    </div>
    <div class="flex-fill border rounded {{(Arr::exists($vars,'v_icms_s')?'':'d-none')}}"  id="v_icms_s">
    <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      VALOR DO ICMS SUBSTITUIÇÃO</strong>
             <p class=" " style='font-size: 15px'></p>
      </div>        
    </div>
    <div class="flex-fill border rounded {{(Arr::exists($vars,'v_pis')?'':'d-none')}}" id="v_pis">
    <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      VALOR DO PIS</strong>
             <p class=" " style='font-size: 15px'>{{$nfe->valor_PIS}}</p>
      </div>        
    </div>
    <div class="flex-fill border rounded {{(Arr::exists($vars,'v_total')?'':'d-none')}}"  id="v_total">
    <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      VALOR TOTAL DOS PRODUTOS</strong>
             <p class=" " style='font-size: 15px'>{{$nfe->valor_produtos}}</p>
      </div>        
    </div>
</div>
<div class=" d-flex justify-content-around text-center" >
    <div class="flex-fill border rounded {{(Arr::exists($vars,'v_frete')?'':'d-none')}}" id="v_frete">
    <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      VALOR DO FRETE</strong>
             <p class=" " style='font-size: 15px'></p>
      </div>        
    </div>
    <div class="flex-fill border rounded {{(Arr::exists($vars,'v_seguro')?'':'d-none')}}"  id="v_seguro">
    <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">    VALOR DO SEGURO</strong>
             <p class=" " style='font-size: 15px'></p>
      </div>        
    </div>
    <div class="flex-fill border rounded {{(Arr::exists($vars,'desc')?'':'d-none')}}" id="desc">
    <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      DESCONTO</strong>
             <p class=" " style='font-size: 15px'></p>
      </div>        
    </div>
    <div class="flex-fill border rounded {{(Arr::exists($vars,'v_outros')?'':'d-none')}}"  id="v_outros">
    <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      OUTRAS DESPESAS</strong>
             <p class=" " style='font-size: 15px'></p>
      </div>        
    </div>
    <div class="flex-fill border rounded {{(Arr::exists($vars,'v_ipi')?'':'d-none')}}" id="v_ipi">
    <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      VALOR TOTAL DO IPI</strong>
             <p class=" " style='font-size: 15px'>{{$nfe->valor_IPI}}</p>
      </div>        
    </div>
    <div class="flex-fill border rounded {{(Arr::exists($vars,'v_cofins')?'':'d-none')}}"  id="v_cofins">
    <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">      VALOR DO COFINS</strong>
             <p class=" " style='font-size: 15px'>{{$nfe->valor_COFINS}}</p>
      </div>        
    </div>
    <div class="flex-fill border rounded {{(Arr::exists($vars,'v_nf')?'':'d-none')}}" id="v_nf">
    <div class="d-flex flex-column align-items-center flex-wrap">
             
             <strong class=" ">     VALOR TOTAL DA NOTA</strong>
             <p class=" " style='font-size: 15px'>{{$nfe->valor_nf}}</p>
      </div>        
    </div>
</div>
</div>

<br>
<br>
<div class=" row w-100" >
<strong>Produtos</strong>

<div class='d-flex justify-content-around w-100'>

<table class="table table-bordered w-100" style="">
            <tbody><tr>
                <th class="{{Arr::exists($vars,'t_codigo_produto')?'':'d-none'}} t_codigo_produto" >CÓDIGO</th>
                <th class="{{Arr::exists($vars,'t_descricao')?'':'d-none'}} t_descricao" >DESCRIÇÃO</th>
                <th class="{{Arr::exists($vars,'t_ncm')?'':'d-none'}} t_ncm" >NCM/SH</th>
                <th class="{{Arr::exists($vars,'t_cst')?'':'d-none'}} t_cst" >O/CST</th>
                <th class="{{Arr::exists($vars,'t_cfop')?'':'d-none'}} t_cfop" >CFOP</th>
                <th class="{{Arr::exists($vars,'t_un')?'':'d-none'}} t_un" >UN</th>
                <th class="{{Arr::exists($vars,'t_quant')?'':'d-none'}} t_quant" >QUANT</th>
                <th class="{{Arr::exists($vars,'t_v_un')?'':'d-none'}} t_v_un" >VALORUNIT</th>
                <th class="{{Arr::exists($vars,'t_v_total')?'':'d-none'}} t_v_total" >VALORTOTAL</th>
                <th class="{{Arr::exists($vars,'t_bc_icms')?'':'d-none'}} t_bc_icms" >B.CÁLCICMS</th>
                <th class="{{Arr::exists($vars,'t_v_icms')?'':'d-none'}} t_v_icms" >VALORICMS</th>
                <th class="{{Arr::exists($vars,'t_v_ipi')?'':'d-none'}} t_v_ipi" >VALORIPI</th>
                <th class="{{Arr::exists($vars,'t_aliq_icms')?'':'d-none'}} t_aliq_icms" >ALÍQ.ICMS</th>
                <th class="{{Arr::exists($vars,'t_aliq_ipi')?'':'d-none'}} t_aliq_ipi" >ALÍQ.IPI</th>
            </tr>
            @foreach($nfe->produtos as $produto)
            <tr>
                <td class="t_codigo_produto {{Arr::exists($vars,'t_codigo_produto')?'':'d-none'}} center">{{$produto->codigo_produto}}</td>
                <td class="t_descricao {{Arr::exists($vars,'t_descricao')?'':'d-none'}} center">{{$produto->nome_produto}}</td>
                <td class="t_ncm {{Arr::exists($vars,'t_ncm')?'':'d-none'}} center">{{$produto->ncm}}</td>
                <td class="t_cst {{Arr::exists($vars,'t_cst')?'':'d-none'}} center"></td>
                <td class="t_cfop {{Arr::exists($vars,'t_cfop')?'':'d-none'}} center"></td>
                <td class="t_un {{Arr::exists($vars,'t_un')?'':'d-none'}} center">{{$produto->unidade_produto}}</td>
                <td class="t_quant {{Arr::exists($vars,'t_quant')?'':'d-none'}} center">{{$produto->quantidade_comprada}}</td>
                <td class="t_v_un {{Arr::exists($vars,'t_v_un')?'':'d-none'}} center">{{$produto->valor_unitario}}</td>
                <td class="t_v_total {{Arr::exists($vars,'t_v_total')?'':'d-none'}} center">{{$produto->valor_produto}}</td>
                <td class="t_bc_icms {{Arr::exists($vars,'t_bc_icms')?'':'d-none'}} center">{{$produto->base_icms}}</td>
                <td class="t_v_icms {{Arr::exists($vars,'t_v_icms')?'':'d-none'}} center">{{$produto->valor_icms}}</td>
                <td class="t_v_ipi {{Arr::exists($vars,'t_v_ipi')?'':'d-none'}} center"></td>
                <td class="t_aliq_icms {{Arr::exists($vars,'t_aliq_icms')?'':'d-none'}} center">{{$produto->aliq_icms}}</td>
                <td class="t_aliq_ipi {{Arr::exists($vars,'t_aliq_ipi')?'':'d-none'}} center"></td>
                

            </tr>
            @endforeach
        </tbody>
        </table>
        </div>
        </div>
                                    </p>
<div class="data_relatorio  h-25" style="padding-top: 12px;">
<div class="row my-3 ">
<strong>DADOS ADICIONAIS</strong>
<div class=" d-flex justify-content-around text-center flex-row h-100" >

<div class="flex-fill border rounded h-100">

</div>
<div class="flex-fill border rounded h-100">
</div>
</div>
</div>
        </div>
</div>
        <div class="page-break"></div>
        @endforeach