@include('template.relatorioHeader')
<div id="container" style='font-size:10px'>
                                        

<div class="row my-3" >

        
<div class=" d-flex flex-wrap justify-content-around text-center " >

     
      <div class='flex-fill w-25 border rounded' id="nome_emit">NATUREZA DA OPERAÇÃO
</div>
   
       <div class='flex-fill w-25 border rounded' id="cnpj_emit">PROTOCOLO DE AUTORIZAÇÃO DE USO
</div>
   
       <div class='flex-fill w-25 border rounded' id="end_emit">INSCRIÇÃO ESTADUAL
</div>
   
       <div class='flex-fill w-25 border rounded' id="bairro_emit">INSCRIÇÃO ESTADUAL DO SUBST. TRIBUT
</div>
   
    </div>

</div>




<div class="row my-3" >

<strong> Área do Destinatário / Remetente</strong>
        
<div class=" d-flex flex-wrap justify-content-around text-center " >

     
      <div class='flex-fill w-25 border rounded' id="nome_dest">NOME / RAZÃO SOCIAL
</div>
   
       <div class='flex-fill w-25 border rounded' id="cnpj_dest">CNPJ / CPF
</div>
   
       <div class='flex-fill w-25 border rounded' id="end_dest">ENDEREÇO
</div>
   
       <div class='flex-fill w-25 border rounded' id="bairro_dest">BAIRRO / DISTRITO
</div>
   
       <div class='flex-fill w-25 border rounded' id="cep_dest">CEP
</div>
   
       <div class='flex-fill w-25 border rounded' id="saida_dest">DATA DA SAÍDA
</div>
   
       <div class='flex-fill w-25 border rounded' id="mun_dest">MUNICÍPIO
</div>
   
       <div class='flex-fill w-25 border rounded' id="uf_dest">UF
</div>
   
       <div class='flex-fill w-25 border rounded' id="fone_dest">FONE / FAX
</div>
   
       <div class='flex-fill w-25 border rounded' id="ie_dest">INSCRIÇÃO ESTADUAL
</div>
   
       <div class='flex-fill w-25 border rounded' id="hora_dest">HORA DA SAÍDA
</div>
    </div>

</div>
   
<div class="row my-3" >
    <strong>CÁLCULO DO IMPOSTO</strong>
<div class=" d-flex justify-content-around text-center " >
    <div class='flex-fill border rounded' id="bc_icms">BASE DE CÁLCULO DO ICMS</div>
    <div class='flex-fill border rounded'  id="v_icms">VALOR DO ICMS</div>
    <div class='flex-fill border rounded' id="bc_icms_st">BASE DE CÁLCULO DO ICMS S.T.</div>
    <div class='flex-fill border rounded'  id="v_icms_s">VALOR DO ICMS SUBSTITUIÇÃO</div>
    <div class='flex-fill border rounded' id="v_pis">VALOR DO PIS</div>
    <div class='flex-fill border rounded'  id="v_total">VALOR TOTAL DOS PRODUTOS</div>
</div>
<div class=" d-flex justify-content-around text-center" >
    <div class='flex-fill border rounded' id="v_frete">VALOR DO FRETE</div>
    <div class='flex-fill border rounded'  id="v_seguro">VALOR DO SEGURO</div>
    <div class='flex-fill border rounded' id="desc">DESCONTO</div>
    <div class='flex-fill border rounded'  id="v_outros">OUTRAS DESPESAS</div>
    <div class='flex-fill border rounded' id="v_ipi">VALOR TOTAL DO IPI</div>
    <div class='flex-fill border rounded'  id="v_cofins">VALOR DO COFINS</div>
    <div class='flex-fill border rounded' id="v_nf">VALOR TOTAL DA NOTA</div>
</div>
</div>

<br>
<br>
<div class="row my-3" >
<strong>Produtos</strong>

<div class='table-responsive-sm '>

<table class="table table-bordered" style="font-size: 4px;width: 60%;">
            <tbody><tr>
                <th class="t_codigo_produto " >CÓDIGO</th>
                <th class="t_descricao " >DESCRIÇÃO</th>
                <th class="t_ncm " >NCM/SH</th>
                <th class="t_cst " >O/CST</th>
                <th class="t_cfop " >CFOP</th>
                <th class="t_un " >UN</th>
                <th class="t_quant " >QUANT</th>
                <th class="t_v_un " >VALORUNIT</th>
                <th class="t_v_total " >VALORTOTAL</th>
                <th class="t_bc_icms " >B.CÁLCICMS</th>
                <th class="t_v_icms " >VALORICMS</th>
                <th class="t_v_ipi " >VALORIPI</th>
                <th class="t_aliq_icms " >ALÍQ.ICMS</th>
                <th class="t_aliq_ipi " >ALÍQ.IPI</th>
            </tr>
            <tr>
                <td class="t_codigo_produto center"></td>
                <td class="t_descricao center"></td>
                <td class="t_ncm center"></td>
                <td class="t_cst center"></td>
                <td class="t_cfop center"></td>
                <td class="t_un center"></td>
                <td class="t_quant center"></td>
                <td class="t_v_un center"></td>
                <td class="t_v_total center"></td>
                <td class="t_bc_icms center"></td>
                <td class="t_v_icms center"></td>
                <td class="t_v_ipi center"></td>
                <td class="t_aliq_icms center"></td>
                <td class="t_aliq_ipi center"></td>
                

            </tr>
            
        </tbody></table>
        </div>
                                    </p>
<!-- <div class="data_relatorio  h-25" style="padding-top: 12px;">
<div class="row my-3 ">
<strong>DADOS ADICIONAIS</strong>
<div class=" d-flex justify-content-around text-center flex-row h-100" >

<div class="flex-fill border rounded h-100">

</div>
<div class="flex-fill border rounded h-100">
</div>
</div>
</div>
        </div> -->