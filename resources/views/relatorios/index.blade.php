@extends('template.admin_template')
@section('title')
<h2>Relátorio</h2>
@endsection

@section('content')


<div class="row m-auto">
    <div class="col-md-6">
    <form action="/relatorios/pdf" class='form-group d-flex  flex-column justify-content-between h-75' method='POST' target="_blank">
    @csrf

    <div class="form-group row">
    <div class="col">
    <label for="data_min">Data Minima da Nota</label>
    <input class="form-control" type="date" multiple id='data_min' name='data_min'>
</div>
    <div class="col">
    <label for="data_max">Data Máxima da Nota</label>
    <input class="form-control" type="date" multiple id='data_max' name='data_max'>
    </div>
  </div>
  <!-- <div class="form-group">
    <label for="exampleFormControlSelect1">Example select</label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>1</option>
      <option>2</option>
      <option>3</option>
      <option>4</option>
      <option>5</option>
    </select>
  </div>
  <div class="form-group">
    <label for="exampleFormControlSelect2">Example multiple select</label>
    <select multiple class="form-control" id="exampleFormControlSelect2">
      <option>1</option>
      <option>2</option>
      <option>3</option>
      <option>4</option>
      <option>5</option>
    </select>
  </div> -->
  <!-- <div class="form-group">
    <label for="exampleFormControlTextarea1">Example textarea</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
  </div> -->
  <div class="form-group row">

    <div class="col">
    <label for="value_min">Valor Minimo da Nota</label>
    <input class='form-control' type="number" id='value_min'  name="value_min">
    </div>
  <div class="col">
    <label for="value_max">Valor Máxima da Nota</label>
    <input class='form-control' type="number" id='value_max'  name="value_max">
    </div>
  </div>

    <div class="accordion " id="accordionExample">
    <div class="card m-1">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button class="btn btn-default" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Área do Destinatário / Remetente
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
    <div class='d-flex flex-wrap  flex-wrap'>
      <label class="checkbox w-50 h-25 ">
      <input type="checkbox" checked onchange="handleChange(this)" name="nome_dest">NOME / RAZÃO SOCIAL
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleChange(this)" name="cnpj_dest">CNPJ / CPF
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleChange(this)" name="end_dest">ENDEREÇO
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleChange(this)" name="bairro_dest">BAIRRO / DISTRITO
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleChange(this)" name="cep_dest">CEP
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleChange(this)" name="saida_dest">DATA DA SAÍDA
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleChange(this)" name="mun_dest">MUNICÍPIO
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleChange(this)" name="uf_dest">UF
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleChange(this)" name="fone_dest">FONE / FAX
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleChange(this)" name="ie_dest">INSCRIÇÃO ESTADUAL
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleChange(this)" name="hora_dest">HORA DA SAÍDA
    </label>
    
    </div>
    </div>
  </div>
  <div class="card m-1">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-default" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Aréa de Imposto
        </button>
      </h5>
    </div>

    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="card-body ">
      <!-- value 1<input type="checkbox">
      value 2<input type="checkbox"> -->
    <div class='d-flex flex-wrap  flex-wrap'>
      <label class="checkbox w-50 h-25 ">
      <input type="checkbox" checked onchange="handleChange(this)" name="bc_icms">BASE DE CÁLCULO DO ICMS
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleChange(this)" name="v_icms">VALOR DO ICMS
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleChange(this)" name="bc_icms_st">BASE DE CÁLCULO DO ICMS S.T.
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleChange(this)" name="v_icms_s">VALOR DO ICMS SUBSTITUIÇÃO
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleChange(this)" name="v_pis">VALOR DO PIS
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleChange(this)" name="v_total">VALOR TOTAL DOS PRODUTOS
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleChange(this)" name="v_frete">VALOR DO FRETE
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleChange(this)" name="v_seguro">VALOR DO SEGURO
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleChange(this)" name="desc">DESCONTO
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleChange(this)" name="v_outros">OUTRAS DESPESAS
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleChange(this)" name="v_ipi">VALOR TOTAL DO IPI
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleChange(this)" name="v_cofins">VALOR DO COFINS
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleChange(this)" name="v_nf">VALOR TOTAL DA NOTA
    </label>
    </div>
      </div>
    </div>
  </div>
  
  <div class="card m-1">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-default" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Área Produtos
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
      <div class='d-flex flex-wrap  flex-wrap'>
      <label class="checkbox w-50 h-25 ">
      <input type="checkbox" checked onchange="handleTableChange(this)" name="t_codigo_produto">CÓDIGO PRODUTO
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleTableChange(this)" name="t_descricao">DESCRIÇÃO DO PRODUTO / SERVIÇO
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleTableChange(this)" name="t_ncm">NCM/SH
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleTableChange(this)" name="t_cst">O/CST
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleTableChange(this)" name="t_cfop">CFOP
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleTableChange(this)" name="t_un">UN
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleTableChange(this)" name="t_quant">QUANT
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleTableChange(this)" name="t_v_un">VALORUNIT
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleTableChange(this)" name="t_v_total">VALORTOTAL
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleTableChange(this)" name="t_bc_icms">B.CÁLCICMS
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleTableChange(this)" name="t_v_icms">VALORICMS
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleTableChange(this)" name="t_v_ipi">VALORIPI
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleTableChange(this)" name="t_aliq_icms">ALÍQ.ICMS
    </label>
    <label class="checkbox w-50 h-25">
      <input type="checkbox" checked onchange="handleTableChange(this)" name="t_aliq_ipi">ALÍQ.IPI
    </label>
    
    </div>     
   </div>
    </div>
  </div>
</div>
    <button class='btn btn-secondary' type='submit'>Emitir Relátorio</button>
    </form>

    </div>
    <idv class="col-md-6 h-100" id='pre-relatorio'>
        @include('relatorios.relatorioPre')
    </idv>
    </div>

@endsection

<script>

    function handleChange(check){
        let id = '#'+check.name
        $('#pre-relatorio').find(id).toggleClass('d-none')
    }
    function handleTableChange(check){
        let id = '.'+check.name
        $('#pre-relatorio').find(id).toggleClass('d-none')
    }

</script>