<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',function(){
    return redirect('/home');
});

Route::get('/login','LoginController@index');
Route::get('/cadastro','LoginController@create');
Route::post('/cadastro','LoginController@cadastrar');
Route::post('/login','LoginController@login');
Route::get('/logout','LoginController@logout');




//middleware auth
Route::middleware([AuthMid::class])->group(function(){

Route::get('/home', 'FileController@input');
Route::post('/upload', 'FileController@upload');

Route::resource('/notas','NfeController');
Route::resource('/notas/produtos','NotasProdutosController');

Route::get('/formNfe',function(){
});

Route::get('/relatorios', 'RelatorioController@index');
Route::post('/relatorios/pdf', 'RelatorioController@MakePdf');
Route::get('/relatorios/teste', 'RelatorioController@testePdf');

});