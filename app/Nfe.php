<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nfe extends Model
{
    protected $fillable = [
        'descricao',
        'quantidade_comprada',
        'data_emissao',  
        'valor_produtos',
        'valor_ICMS',
        'valor_IPI',
        'valor_PIS',
        'valor_COFINS',
        'valor_nf',
        'emit_id',
        'dest_id'
    ];
    protected $table = 'nfes';

    protected function emissor(){
        return $this->belongsTo(Empresa::class,'emit_id');
    }

    protected function dest(){
        return $this->belongsTo(Empresa::class,'dest_id');
    }
    protected function produtos(){
        return $this->hasMany(NfeProduto::class,'nfe_id');
    }
}
