<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nfe;
use PDF;
use Session;
class RelatorioController extends Controller
{
    function index(){
        return view('relatorios.index');
    }

    function MakePdf(Request $request){
        $query = Nfe::select();

        if($request->post('data_min')){
            $query->where('data_emissao','>',$request->post('data_min'));
        }
        if($request->post('data_max')){
            $query->where('data_emissao','<',$request->post('data_max'));
        }
        if($request->post('value_min')){
            $query->where('valor_nf','>',$request->post('value_min'));
        }
        if($request->post('value_max')){
            $query->where('valor_nf','<',$request->post('value_max'));
        }
        $vars = $request->except(['data_min', 'data_max','value_min','value_max']);
        $model = $query->get();
        return view('relatorios.relatorio_padrao',compact('model','vars'));

    }
    
    function testePdf(){
        $query = Nfe::select();
        // if($request->post('data_min')){
        //     $query->where('data_emissao','>',$request->post('data_min'));
        // }
        // if($request->post('data_max')){
        //     $query->where('data_emissao','<',$request->post('data_max'));
        // }
        // if($request->post('valor_min')){
        //     $query->where('valor_nf','>',$request->post('valor_min'));
        // }
        // if($request->post('valor_max')){
        //     $query->where('valor_nf','<',$request->post('valor_max'));
        // }
        
        $model = $query->get();
        return view('relatorios.relatorio_padrao',compact('model'));
    }
}
