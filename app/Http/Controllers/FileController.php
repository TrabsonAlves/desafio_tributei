<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\XmlNfeController;

class FileController extends Controller
{

    function input(){
        return view('input');

    }
    function upload(Request $request){
        foreach($request->file('nfe') as $file){
        new XmlNfeController ($file);
        }
        return redirect('notas');
    }
}
