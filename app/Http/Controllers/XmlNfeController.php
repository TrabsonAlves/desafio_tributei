<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use App\Nfe;
use App\NfeProduto;

use App\Empresa;
use App\Endereco;

class XmlNfeController extends Controller
{
    protected $nfe_id;

    protected $emit_id;

    protected $dest_id;

    protected $path;

    // protected $countItems;

     function __construct($file){
        //  $this->countItems = 0;

        $this->path = $file->store('/nfe');

        $xmlString = file_get_contents(storage_path('app/'.$this->path));

        $xmlObject = simplexml_load_string($xmlString);

        $json = json_encode($xmlObject);

        $phpArray = json_decode($json, true); 

        $complete = completeXml($phpArray);

        $this->nfe_xml2db($phpArray);
    }

    protected function nfe_emitEnd_create($emitEnd){
        $end = [
            'cep'=>$emitEnd['CEP'],
            'Logradouro'=>$emitEnd['xLgr'],
            'numero'=>$emitEnd['nro'],
            'cpl'=>$emitEnd['xCpl'],
            'bairro'=>$emitEnd['xBairro'],
            'municipio'=>$emitEnd['xMun'],
            'Uf'=>$emitEnd['UF'],
            'pais'=>$emitEnd['xPais'],
            'telefone'=>$emitEnd['fone'],
            'email'=>$emitEnd['Email']??null,
            'empresa_id'=>$this->emit_id
        ];
        Endereco::FirstOrCreate($end)->id;
        

}

protected function nfe_destEnd_create($destEnd){
    $end = [
        'cep'=>$destEnd['CEP_dest'],
        'Logradouro'=>$destEnd['xLgr_dest'],
        'numero'=>$destEnd['nro_dest'],
        'cpl'=>$destEnd['xCpl_dest'],
        'bairro'=>$destEnd['xBairro_dest'],
        'municipio'=>$destEnd['xMun_dest'],
        'Uf'=>$destEnd['UF_dest'],
        'pais'=>$destEnd['xPais_dest'],
        'telefone'=>$destEnd['fone_dest'],
        'email'=>$destEnd['xEmail_dest']??null,
        'empresa_id'=>$this->dest_id
    ];
    Endereco::FirstOrCreate($end)->id;
    

}

        protected function nfe_emit_create($emit){
                $emit_item = [
                    'nome'=>$emit['xNome'],
                    'nome_fantasia'=>$emit['xFant'],
                    'IE'=>$emit['IE'],
                    'CRT'=>$emit['CRT'],
                    'CNPJ'=>$emit['CNPJ_emit']
                ];
                $this->emit_id = Empresa::UpdateOrCreate($emit_item)->id;
                $this->nfe_emitEnd_create($emit['enderEmit']);
        
    }

        protected function nfe_dest_create($dest){
                $dest_item = [
                    'nome'=>$dest['xNome_dest'],
                    'nome_fantasia'=>$dest['xFant']??null,
                    'IE'=>$dest['IE_dest']??null,
                    'CRT'=>$dest['CRT']??null,
                    'CNPJ'=>$dest['CNPJ_dest']
                ];
                $this->dest_id = Empresa::UpdateOrCreate($dest_item)->id;
                $this->nfe_destEnd_create($dest['enderDest']);
        
    }
        protected function nfe_det_create($det){
            foreach($det as $detItem){
             $atual = completeDetXml($detItem);
            $prod = [
                'codigo_produto'=>$atual['prod']['cProd'],
                'nome_produto'=>$atual['prod']['xProd'],
                'valor_unitario'=>(float)$atual['prod']['vUnCom'],
                'ncm'=>$atual['prod']['NCM'],
                'quantidade_comprada'=>(int)$atual['prod']['qCOM'],
                'valor_produto'=>(float)$atual['prod']['vProd'],
                // 'EAN'=>$atual['prod']['cEAN'],
                'unidade_produto'=>$atual['prod']['uCOM'],
                'base_icms'=>$atual['imposto']['ICMS']['vBC']??null,
                'aliq_icms'=>$atual['imposto']['ICMS']['pICMS']??null,
                'valor_icms'=>$atual['imposto']['ICMS']['vICMS_icms']??null,
                'nfe_id'=>(int)$this->nfe_id
            ];
            $prod_id = NfeProduto::FirstOrCreate($prod);
            // $this->countItems = $this->countItems + 1;
        }
        }
    
        protected function nfe_xml2db($xml){

            $this->nfe_emit_create($xml['emit']);
            $this->nfe_dest_create($xml['dest']);
            $nfe = [
                'descricao'=>$xml['ide']['natOp'],
                'quantidade_comprada'=>(int)0,
                'data_emissao'=>$xml['ide']['dhEmi'],
                'valor_produtos'=>(float)$xml['total']['ICMStot']['vProd_ttlnfe'],
                'valor_ICMS'=>(float)$xml['total']['ICMStot']['vICMS_ttlnfe'],
                'valor_IPI'=>(float)$xml['total']['ICMStot']['vIPI_ttlnfe'],
                'valor_PIS'=>(float)$xml['total']['ICMStot']['vPIS_ttlnfe'],
                'valor_COFINS'=>(float)$xml['total']['ICMStot']['vCOFINS_ttlnfe'],
                'valor_nf'=>(float)$xml['total']['ICMStot']['vNF'],
                'emit_id'=>$this->emit_id,
                'dest_id'=>$this->dest_id
            ];
            $this->nfe_id = Nfe::FirstOrCreate($nfe)->id;
            (array_key_exists('prod',$xml['det']['detItem']))?$this->nfe_det_create($xml['det']):$this->nfe_det_create($xml['det']['detItem']);
        }

        
             function __destruct() {
                
                File::delete(storage_path("app/".$this->path));
        } 
    
}
