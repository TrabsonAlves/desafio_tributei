<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
class LoginController extends Controller
{
    function index(){
        return view('main.login');
    }
    function create(){
        return view('main.cadastro');
    }
    function login(Request $request){
        $valida = [
            'email'=>$request->post('email'),
            'password'=>$request->post('senha')
        ];

    	if (Auth::attempt($valida)) {

            return redirect('/home');

        }else{return redirect('login');}

    }

    function cadastrar(Request $request){
        $valida = [
            'name'=>$request->post('name'),
            'email'=>$request->post('email'),
            'password'=>\Hash::make($request->post('senha'))
        ];
        User::create($valida);
    	if (Auth::attempt($valida)) {

            return redirect('/home');

        }else{return redirect('login');}

    }

    public function logout(Request $request) {

        Auth::logout();
      
        return redirect('/');
      }
}
