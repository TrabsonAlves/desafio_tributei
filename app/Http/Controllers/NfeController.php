<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nfe;
use App\Empresa;
class NfeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nfe = Nfe::all();
        return view('nfe.index',['nfes'=>$nfe]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $nfe = Nfe::find($id);
        return view('nfe.show',compact('nfe'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $nfe = [
            'descricao'=>$request->post('descricao'),
            'data_emissao'=>$request->post('data_emissao'),
            'valor_produtos'=>$request->post('valor_produtos'),
            'valor_ICMS'=>$request->post('valor_ICMS'),
            'valor_nf'=>$request->post('valor_nf'),
            'valor_IPI'=>$request->post('valor_IPI'),
            'valor_PIS'=>$request->post('valor_PIS'),
            'valor_COFINS'=>$request->post('valor_COFINS')
        ];
        $emit=[
            'nome'=>$request->post('nome_emit'),
            'nome_fantasia'=>$request->post('nome_fantasia_emit'),
            'IE'=>$request->post('IE_emit'),
            'cnpj'=>$request->post('cnpj_emit')
            
        ];
        $dest=[
            'nome'=>$request->post('nome_dest'),
            'nome_fantasia'=>$request->post('nome_fantasia_dest'),
            'IE'=>$request->post('IE_dest'),
            'cnpj'=>$request->post('cnpj_dest')
            
        ];
        $fks = Nfe::find($id);
        $fks->update($nfe);
        Empresa::find($fks->emit_id)->update($emit);
        Empresa::find($fks->dest_id)->update($dest);
        return redirect('notas/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Nfe::destroy($id);
        return redirect('/notas');
    }
}
