<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NfeProduto;
class NotasProdutosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produtos = NfeProduto::where('nfe_id',$id)->get();
        return view('produtos.index',['produtos'=>$produtos]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $produto = NfeProduto::find($id);
        return view('produtos.form',['produto'=>$produto]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $nfe_produto = [
            'codigo_produto'=>$request->post('codigo_produto'),
            'nome_produto'=>$request->post('nome_produto'),
            'valor_unitario'=>$request->post('valor_unitario'),
            'unidade_produto'=>$request->post('unidade_produto'),
            'valor_produto'=>$request->post('valor_produto'),
            'base_icms'=>$request->post('base_icms'),
            'aliq_icms'=>$request->post('aliq_icms'),
            'valor_icms'=>$request->post('valor_icms')
        ];
        $nfe = NfeProduto::find($id);
        $nfe->update($nfe_produto);
        return redirect('notas/produtos/'.$nfe->nfe_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        NfeProduto::destroy($id);
        return redirect('/notas');
    }
}
