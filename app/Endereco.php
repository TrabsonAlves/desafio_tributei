<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    protected $fillable = [
        'cep',
        'Logradouro',
        'numero',
        'cpl',
        'bairro',
        'municipio',
        'Uf',
        'pais',
        'telefone',
        'email',
        'empresa_id'
    ];
    protected function getEnderecoFullAttribute(){
        return $this->Logradouro.", ".$this->numero." - ".$this->cpl;
    }
}
