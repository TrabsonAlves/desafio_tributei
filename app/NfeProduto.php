<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NfeProduto extends Model
{
    protected $fillable = [
        'codigo_produto',
        'nome_produto',
        'valor_unitario',
        'ncm',
        'quantidade_comprada',
        'valor_produto',
        'EAN',
        'nfe_id',
        'unidade_produto',
        'base_icms',
        'aliq_icms',
        'valor_icms'
    ];
}
