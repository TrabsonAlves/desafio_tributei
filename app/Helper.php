<?php

use App\Nfe;
use App\NfeProduto;




if(!function_exists('completeXml')){
    function completeXml($initial){
        $xmlString = file_get_contents(storage_path('app/nfe/XML_Todos_Campos_40.xml'));

        $xmlObject = simplexml_load_string($xmlString);

        $json = json_encode($xmlObject);

        $phpArray = json_decode($json, true); 

        return array_merge($phpArray,$initial);

    }
}

if(!function_exists('completeDetXml')){
    function completeDetXml($initial){
        $xmlString = file_get_contents(storage_path('app/nfe/XML_Todos_Campos_40.xml'));

        $xmlObject = simplexml_load_string($xmlString);

        $json = json_encode($xmlObject);

        $phpArray = json_decode($json, true); 

        return array_merge($phpArray['det']['detItem'],$initial);

    }
}