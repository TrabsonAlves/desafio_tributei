<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $fillable = [
        'nome',
        'nome_fantasia',
        'IE',
        'CRT',
        'CNPJ'
    ];
    protected function endereco(){
        return $this->hasOne(Endereco::class,'empresa_id');
    }
}
