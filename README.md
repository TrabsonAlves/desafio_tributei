Repositório destinada áá desafio de prototipagem da Tributei

Sistema com disposição de: 

- login / cadastro de usuários, aléém de barrar entradas indevidas(não logadas);

- Upload de arquivos de xml de notas ficais eletronicas( **padrão de versão 4.0**) para importação de informações para o sistema;

- Consulta de notas e produtos por nota, importados pelo upload, além da edição dos dados salvos;

- Emissão de relatório com base em algumas condições prévias opcionais, como data miníma de emissão e valor máximo de nota,
além de ser possivel remover alguns campos que são trazidos por padrão

**Informações Úteis:**

Link do servidor em nuvem [aqui](https://desafiotributeirafael.000webhostapp.com/login)

Usuário adm para teste: administrador@gmail.com

Senha do Usuário adm: rootAdmin


**Notas:**
1. O sistema nem o banco salvam ainda todas as informações da nota, pro banco foi mapeado previamente os campos mais obrigatórios e comuns para ser desenvolvido no tempo. Quanto ao relatório, ele foi baseado no arquivo DANFE que iráá junto com outros exemplos .

2. Infelizmente, não é possivel atualmente no projeto alterar informações cadastrais, nem informações do contribuente logado. Sendo assim não vindo junto ao relatório.

3. Também não foi possivel mapear 100¢ dos impostos referentes as informações registradas, logo não foi possivel calcular corretamente os tributos envolvidos.

4. Algumas informações dos exemplos já foram salvas para teste em nuvem

5. Sistema hospedado pela webhost, por ser uma opção de prototipagem gratuita, para este desafio nãão vi a necessidade de uma hospedagem com muito custo envolvido

Agradeço pela compreensãão e pela oportunidade, como listado, ainda existe muita oportunidade de melhora para o projeto atual, mas acredito que consegui entregar meu melhor nesse momento para solucionar essa demanda.

> Rafael da Silva Alves
