<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ForeignCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('nfes', function (Blueprint $table) {
            $table->foreign('emit_id')->references('id')->on('empresas');
            $table->foreign('dest_id')->references('id')->on('empresas');

        });
        Schema::table('enderecos', function (Blueprint $table) {
            $table->foreign('empresa_id')->references('id')->on('empresas');
        });
        Schema::table('nfe_produtos', function (Blueprint $table) {
            $table->foreign('nfe_id')->references('id')->on('nfes');
        });
        // Schema::table('unidades', function (Blueprint $table) {
        //     $table->foreign('entidade_id')->references('id')->on('entidades');
        // });
        // Schema::table('unidades', function (Blueprint $table) {
        //     $table->foreign('entidade_id')->references('id')->on('entidades');
        // });
        // Schema::table('unidades', function (Blueprint $table) {
        //     $table->foreign('entidade_id')->references('id')->on('entidades');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nfes', function (Blueprint $table) {
            $table->dropColumn('emit_id');
            $table->dropColumn('dest_id');

        });
        Schema::table('enderecos', function (Blueprint $table) {
            $table->dropColumn('empresa_id');
        });
        Schema::table('nfe_produtos', function (Blueprint $table) {
            $table->dropColumn('nfe_id');
        });
    }
}
