<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNfeProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nfe_produtos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('nfe_id');
            $table->string('codigo_produto');
            $table->string('nome_produto');
            $table->float('valor_unitario');
            $table->string('ncm');
            $table->float('quantidade_comprada');
            $table->string('unidade_produto');
            $table->float('valor_produto');
            $table->float('base_icms')->nullable();
            $table->float('aliq_icms')->nullable();
            $table->float('valor_icms')->nullable();
            $table->string('EAN')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nfe_produtos');
    }
}
