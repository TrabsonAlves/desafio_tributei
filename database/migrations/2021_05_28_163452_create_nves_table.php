<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNvesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nfes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descricao');
            $table->integer('quantidade_comprada');
            $table->date('data_emissao');
            $table->integer('emit_id');
            $table->integer('dest_id');
            $table->float('valor_produtos');
            $table->float('valor_ICMS');
            $table->float('valor_IPI');
            $table->float('valor_PIS');
            $table->float('valor_COFINS');
            $table->float('valor_nf');

            // $table->string('pagamento');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nfes');
    }
}
