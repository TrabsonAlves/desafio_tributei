<?php

use Illuminate\Database\Seeder;
use App\User;
class AuthSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $autenticacoes=[
        [
            'name'=>'Rafael Alves',
            'email' => 'rafael@gmail.com',
            'password' => \Hash::make('rafa123')
        ],
        [
            'name'=>'Administrador',
            'email' => 'administrador@gmail.com',
            'password' => \Hash::make('rootAdmin')
        ]
    ];

    foreach($autenticacoes as $autenticacao){
    User::firstOrCreate($autenticacao);
    }
}
}
